var StellarSdk = require('stellar-sdk');

StellarSdk.Network.useTestNetwork();
var server = new StellarSdk.Server('https://horizon-testnet.stellar.org');
var issuingKeys = StellarSdk.Keypair
  .fromSecret('SCHB4TVRTNEHT55SJXVYDTXX5FHLRHV3LZ6YVV3YYURNXW2JFZOX7X2S');
var receivingKeys = StellarSdk.Keypair
  .fromSecret('SC2GBEZLYVFGCTSVEQQRLTS7RQUE4LC6NEV5QDWPXU2RPWD4JEQLSP3A');

  var receivingKeys2 = StellarSdk.Keypair
  .fromSecret('SAU2OITBMVE3UZEN3IEG6RT5GWIH4MAE7BMOVT6LA7KRYHX5STCOFUFI');

var astroDollar = new StellarSdk.Asset('MkToken', issuingKeys.publicKey());
console.log("token details:",astroDollar);

function tokenTransfering(){
server.loadAccount(receivingKeys.publicKey())
  .then(function(receiver) {
    console.log("receiver callback::",receiver.balances);
    var transaction = new StellarSdk.TransactionBuilder(receiver)
      .addOperation(StellarSdk.Operation.changeTrust({
        asset: astroDollar,
        limit: '1000'
      }))
      .setTimeout(100)
      .build();
    transaction.sign(receivingKeys);
    return server.submitTransaction(transaction);
  })
  .then(function() {
    return server.loadAccount(issuingKeys.publicKey())
  })
  .then(function(issuer) {
    console.log("issuer",issuer.balances);
    
    var transaction = new StellarSdk.TransactionBuilder(issuer)
      .addOperation(StellarSdk.Operation.payment({
        destination: receivingKeys.publicKey(),
        asset: astroDollar,
        amount: '3'
      }))    
      .setTimeout(100)
      .build();
    transaction.sign(issuingKeys);
    return server.submitTransaction(transaction);
  })
  .catch(function(error) {
    console.error('Error!', error);
  });
}

// tokenTransfering()


//domain setting
function issuerDomainsetting(){
    server.loadAccount(issuingKeys.publicKey())
  .then(function(issuer) {
      // console.log("Issuer",issuer.balances);
      
    var transaction = new StellarSdk.TransactionBuilder(issuer)
      .addOperation(StellarSdk.Operation.setOptions({
        homeDomain: 'yourdomain.com',
      }))
      .setTimeout(100)
      .build();
    transaction.sign(issuingKeys);
    return server.submitTransaction(transaction);
  })

  //requiring an authorization

.then(function(issuingAccount){
  StellarSdk.Network.useTestNetwork();
var transaction = new StellarSdk.TransactionBuilder(issuingAccount)
  .addOperation(StellarSdk.Operation.setOptions({
    setFlags: StellarSdk.AuthRevocableFlag | StellarSdk.AuthRequiredFlag
  }))
  .setTimeout(100)
  .build();
transaction.sign(issuingKeys);
return server.submitTransaction(transaction);
})
.catch(function(error) {
    console.error('Error!', error);
  });
}

// issuerDomainsetting()

function trustcheck(){
    var astroDollarCode = 'AstroDollar';
var astroDollarIssuer = issuingKeys.publicKey();
// var accountId = receivingKeys.publicKey();
server.loadAccount(receivingKeys2.publicKey()).then(function(account) {
  var trusted = account.balances.some(function(balance) {
    return balance.asset_code === astroDollarCode &&
           balance.asset_issuer === astroDollarIssuer;
  });

  console.log(trusted ? 'Trusted :)' : 'Not trusted :(');
});
}

// trustcheck()

function transferTokenFromReceiver(){
  server.loadAccount(receivingKeys2.publicKey())
  .then(function(receiver) {
    console.log("receiver2 balance:",receiver.balances);
    var transaction = new StellarSdk.TransactionBuilder(receiver)
      .addOperation(StellarSdk.Operation.changeTrust({
        asset: astroDollar,
        limit: '1000'
      }))
      .setTimeout(100)
      .build();
    transaction.sign(receivingKeys2);
    return server.submitTransaction(transaction);
  })
  .then(function() {
    return server.loadAccount(receivingKeys.publicKey())
  })
  .then(function(issuer) {
  console.log("issuer2 balance:",issuer.balances);

    var transaction = new StellarSdk.TransactionBuilder(issuer)
      .addOperation(StellarSdk.Operation.payment({
        destination: receivingKeys2.publicKey(),
        asset: astroDollar,
        amount: '3'
      }))    
      .setTimeout(100)
      .build();
    transaction.sign(receivingKeys);
    return server.submitTransaction(transaction);
  })
  .catch(function(error) {
    console.error('Error!', error);
  });
}

// transferTokenFromReceiver();
